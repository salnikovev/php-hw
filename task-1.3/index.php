<?php

/* First task */
$animalsByContinents = [
    "Eurasia" => [
        "Sorex",
        "Podoces"
    ],
    "North America" => [
        "Lepus",
        "Lanius isabellinus"
    ],
    "South America" => [
        "Aegialites cantianus"
    ],
    "Africa" => [
        "Carpodacus erythrinus",
        "Anas acuta",
        "Turtur",
        "Corvus frugilegus"
    ],
    "Australia" => [
        "Felis lynx",
        "Graculus carbo"
    ]
];


/* Second task */
function findShortNames($animalsByContinent, &$surnames)
{
    $newAnimals = array();

    foreach ($animalsByContinent as $continent => $animals) {
        $newAnimals[$continent] = array();
        foreach ($animals as $name) {
            if (count(explode(" ", $name)) == 2) {
                $newAnimals[$continent][] = $name;
                $surnames[] = explode(" ", $name)[1];
            }
        }
    }
    return $newAnimals;
}

/* Third task*/
function shuffleAnimals($animalsByContinent, &$surnames)
{
    shuffle($surnames);
    /* присваивание новых имен */
    foreach ($animalsByContinent as $continent => &$animals) {
        foreach ($animals as &$name) {
            $name = explode(" ", $name)[0] . " " . array_shift($surnames);
        }
    }
    return $animalsByContinent;
}

/* view */
echo "<h1>Домашнее задание 1.3 </h1>";
echo "<h2>1. Создаем массив животных сгруппированных по континентам</h2>";
foreach ($animalsByContinents as $continent => $animals) {
    if (count($animals) > 0) {
        echo "<h3>$continent</h3>";
    }
    echo "<ul>";
    foreach ($animals as $animal) {
        echo "<li>$animal</li>";
    }
    echo "</ul>";
}

$surnames = array();
$task2 = findShortNames($animalsByContinents, $surnames);
echo "<h2>2. Оставляем животных только с фамилией</h2>";
foreach ($task2 as $continent => $animals) {
    if (count($animals) > 0) {
        echo "<h3>$continent</h3>";
    }
    echo "<ul>";
    foreach ($animals as $animal) {
        echo "<li>$animal</li>";
    }
    echo "</ul>";
}

$task3 = shuffleAnimals($task2, $surnames);
echo "<h2>3. Перемешиваем фамилии животных</h2>";
foreach ($task3 as $continent => $animals) {
    if (count($animals) > 0) {
        echo "<h3>$continent</h3>";
        echo "<ul><li>" . implode(", ", $animals) . "</li></ul>";
    }

}