<?
$userName = 'Евгений';
$userAge = 27;
$userEmail = 'drsalliery@inbox.ru';
$userCity = 'Санкт-Петербург';
$userAbout = 'Разработчик ECMAScript';
$title = $userName . " " . $userAbout;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><? echo $title ?></title>
    <meta charset="utf-8">
    <style>
        body {
            font-family: sans-serif;
        }

        dl {
            display: table-row;
        }

        dt, dd {
            display: table-cell;
            padding: 5px 10px;
        }
    </style>
</head>
<body>
<h1>Страница пользователя <? echo $userName ?></h1>
<dl>
    <dt>Имя</dt>
    <dd><? echo $userName ?></dd>
</dl>
<dl>
    <dt>Возраст</dt>
    <dd><? echo $userAge ?></dd>
</dl>
<dl>
    <dt>Адрес электронной почты</dt>
    <dd><a href="mailto:<? echo $userEmail ?>"><? echo $userEmail ?></a></dd>
</dl>
<dl>
    <dt>Город</dt>
    <dd><? echo $userCity ?></dd>
</dl>
<dl>
    <dt>О себе</dt>
    <dd><? echo $userAbout ?></dd>
</dl>
</body>
</html>
