<?php

function wrap($text)
{
    static $numberOfRow = 1;
    echo "<dl>
               <dt>Шаг " . $numberOfRow++ . "</dt>
               <dd>$text</dd>
           </dl>";
}

function fibonacci($number)
{
    wrap("Запуск функции");
    if ($number) {
        wrap("Пользователь ввел $number");
    } else {
        wrap("Некорректный ввод. Формат ввода: url?number=Ваше число");
    }

    $one = 1;
    $two = 1;
    do {
        if ($number < $one) {
            wrap("Задуманное число НЕ входит в числовой ряд");
            break;
        } elseif ($number == $one) {
            wrap("Задуманное число входит в числовой ряд");
            break;
        } else {
            $three = $one;
            $one += $two;
            $two = $three;
            wrap("Первое число: $one 
                      Второе число: $two 
                      Вспомогательная переменная $three");
        }
    } while ($number > $two);
    wrap("Конец работы функции");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Числа фибоначчи</title>
    <meta charset="utf-8">
    <style>
        body {
            font-family: sans-serif;
        }

        dl {
            display: table-row;
        }

        dt, dd {
            display: table-cell;
            padding: 5px 10px;
        }
    </style>
</head>
<body>
<?
fibonacci($_GET['number']);
?>
</body>
</html>
