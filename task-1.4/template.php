<?php

function renderHtml($data)
{
    $city = $data["name"];
    $mainDescription = $data["weather"][0]["main"];
    $weatherDescription = $data["weather"][0]["description"];
    $icon = $data["weather"][0]["icon"];
    $temp = $data["main"]["temp"];
    $pressure = $data["main"]["pressure"];
    $humidity = $data["main"]["humidity"];
    $windSpeed = $data["wind"]["speed"];
    $today = date("d M Y", $data["dt"]);
    $suriseTime = date("H:i:s", $data["sys"]["sunrise"]);
    $sunsetTime = date("H:i:s", $data["sys"]["sunset"]);
    $coordLat = $data["coord"]["lat"];
    $coordLon = $data["coord"]["lon"];

    return
        '
<html>
    <head>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="widget__layout">
            <div class="widget__temperature">
                <span>
                    <div id="weather-widget" class="weather-widget">
                        <h2 class="weather-widget__city-name">Weather in ' . $city . '</h2>
                        <h3 class="weather-widget__temperature">
                            <img class="weather-widget__img" src="https://openweathermap.org/img/w/' . $icon . '.png" alt="Weather Saint '. $city . '" width = "50" height = "50" >
                            ' . $temp . '
                        </h3>
                        <p class="weather-widget__main"> ' . $weatherDescription . ' </p>
                        <p>' . $today . '</p>
                        <table class="weather-widget__items">
                            <tbody>
                                <tr class="weather-widget__item">
                                    <td>Wind</td>
                                    <td id="weather-widget-wind">speed ' . $windSpeed . ' m / s</td>
                                </tr>
                                <tr class="weather-widget__item">
                                    <td>' . $mainDescription . '</td>
                                    <td id="weather-widget-cloudiness">' . $weatherDescription . '</td>
                                </tr>
                                <tr class="weather-widget__item">
                                    <td>Pressure</td>
                                    <td>' . $pressure . ' hpa</td>
                                </tr>
                                <tr class="weather-widget__item">
                                    <td>Humidity</td>
                                    <td>' . $humidity . '%</td>
                                </tr>
                                <tr class="weather-widget__item">
                                    <td>Sunrise</td>
                                    <td>' . $suriseTime . '</td>
                                </tr>
                                <tr class="weather-widget__item">
                                    <td> Sunset</td>
                                    <td>' . $sunsetTime . '</td>
                                </tr>
                                <tr class="weather-widget__item">
                                    <td> Geo coords </td>
                                    <td>
                                        <a href="https://openweathermap.org/weathermap?zoom=8&amp;lat=' . $coordLat . '&amp;lon=' . $coordLon . '" class="weather-widget__link">[
                                            <span id="wrong-data-lat">' . $coordLat . '</span>,&nbsp;
                                            <span id="wrong-data-lon">' . $coordLon . '</span>]
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </span>    
            </div>  
        </div>
    </body>
</html>';
}