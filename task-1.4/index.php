<?php
include 'template.php';
/*
 *  524901	Moscow
 *  498817	Saint Petersburg
 *  472045	Voronezh
 *
 */
date_default_timezone_set('Europe/Moscow');

$city = 498817;
$key = "39fe89ad3a72b221e1783a25eaa3542d";
$URL = "http://api.openweathermap.org/data/2.5/weather?id=$city&units=metric&appid=$key";
$fileCache = __DIR__ . "\cache.json";


function createCache($URL, $fileCache)
{
    $now = time();
    $timeCache = time();

    if (file_exists($fileCache)) {
        $timeCache = filemtime($fileCache);
    }

    if ( !file_exists($fileCache) || ((($now - $timeCache) / 3600) > 1)) {
        $data = file_get_contents($URL);
        file_put_contents($fileCache, $data);
    }
}

function render($fileCache)
{
    $dataFromCache = json_decode(file_get_contents($fileCache), true);
    echo renderHtml($dataFromCache);;
}

createCache($URL, $fileCache);
render($fileCache);
