<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Football</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h1>Домашнее задание 2.1 «Установка и настройка веб-сервера»</h1>
    <table class="table">
        <tr>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Дата рождения</th>
            <th>Возраст</th>
            <th>Рост</th>
            <th>Вес</th>
            <th>Амплуа</th>
            <th>Номер</th>
            <th>Игры</th>
        </tr>
        <?php
        $players = json_decode(file_get_contents(__DIR__ . "\data.json"), true);
        foreach ($players as $player) {
            ?>
            <tr>
                <td><?php echo $player['firstName'] ?></td>
                <td><?php echo $player['lastname'] ?></td>
                <td><?php echo $player['info']['bornDate'] ?></td>
                <td><?php echo $player['info']['age'] ?></td>
                <td><?php echo $player['info']['height'] ?></td>
                <td><?php echo $player['info']['weight'] ?></td>
                <td><?php echo $player['role'] ?></td>
                <td><?php echo $player['number'] ?></td>
                <td>
                    <?php
                    foreach ($player['lastGames'] as $game) {
                        echo implode(" ", $game) . "<br>";
                    }
                    ?>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>
</body>
</html>